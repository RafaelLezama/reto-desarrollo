const Movie = require('../models/movie');
const fetch = require('node-fetch');

const findController = async (ctx) => {
    // Se reciben las variables recibidas de la URL y headers
    const searchTitle = ctx.request.params.title;
    const year = ctx.request.header.y;
    // Se prepara y reaaliza la busqueda en http://www.omdbapi.com
    const query = 'http://www.omdbapi.com/?t=' + searchTitle + '&y=' + year + '&plot=full&apikey=' + process.env.API_KEY;
    const response = await fetch(query);
    try {
        // Se convierte la respuesta en JSON 
        const body = await response.json()
            .then((res) => {
                // Se verifica el exito de la busqueda en omdbapi.com
                if (res.Response == 'False') {
                    throw console.log(res.Error)
                }

                // se prepara el objeto a guardar en la base de datos local
                let movie = new Movie();
                movie.title = res.Title;
                movie.year = res.Year;
                movie.released = res.Released;
                movie.genre = res.Genre;
                movie.director = res.Director;
                movie.actors = res.Actors;
                movie.plot = res.Plot;
                movie.ratings = res.Ratings;

                // Se elimina el campo _id del objeto para poder actualizarlo
                var movieToUpdate = {};
                movieToUpdate = Object.assign(movieToUpdate, movie._doc);
                delete movieToUpdate._id;

                // Se hace el upsert en la base de datos 
                return Movie.findOneAndUpdate({ title: movie.title, year: movie.year }, movieToUpdate, {
                    useFindAndModify: true,
                    new: true,
                    upsert: true
                });

            })
            .then((resolve) => {
                // Se devuelve el objeto de la base de datos como respuesta
                return ctx.body = resolve;
            })
            .catch(console.error);

    } catch (error) {
        console.log(error);
    }

};

const listController = async (ctx) => {
    // Se reciben la variable page de los headers
    var page = ctx.request.header.page
    if (!page ||
        page == 0 ||
        page == "0" ||
        page == null ||
        page == undefined) {
        var page = 1;
    } else {
        var page = parseInt(ctx.request.header.page);
    }

    // opciones de paginacion 
    const options = {
        limit: 5,
        page: page
    };
    // paginado
    return Movie.paginate({}, options).then((resolve) => {
        // Se devuelve el objeto de la base de datos como respuesta
        ctx.body = resolve;
    });
};
const updateController = async (ctx) => {
    // Se reciben las variables movie, find y replace del body
    const movie = ctx.request.body.movie;
    const find = ctx.request.body.find;
    const replace = ctx.request.body.replace;
    // Se hace la busqueda de la pelicula enla base de datos
    const dbSearch = await Movie.findOne({ title: movie })
        .then((res) => {
            // Verificacion de la busqueda dentro de la base de datos
            if (!res) {
                throw console.log(`la pelicula ${movie} no se encuentra dentro de la base de datos local`)
            }
            const oldPlot = res.plot;
            res.plot = oldPlot.replace(find, replace);
            //  Se realiza la busqueda y modificacion en la base de datos
            return Movie.findOneAndUpdate({ title: res.title }, res, {
                new: true,
                upsert: false,
            });

        })
        .then((resolve) => {
            // Se devuelve el objeto modificado de la base de datos como respuesta
            return ctx.body = resolve;
        })
        .catch(console.error);
};

module.exports = {
    listController,
    findController,
    updateController
}