// Importan los modulos a usar
require('dotenv').config();
const Koa = require('koa');
const app = new Koa();
const mongoose = require('mongoose');
const bodyParser = require('koa-bodyparser');
const router = require('./routes/movie');


// Swagger
const swaggerUI = require('koa2-swagger-ui');
const yamljs = require('yamljs');
const spec = yamljs.load('./swagger.yaml');

// Creacion de ruta para la documentacion con Swagger '/docs/swagger-doc'
router.get('/docs/swagger-doc', swaggerUI.koaSwagger({ routePrefix: false, swaggerOptions: { spec } }));

// Se inicializan body parser y las rutas

app.use(bodyParser());
app.use(router.routes()).use(router.allowedMethods())

// Se hace la conexion a la base de datos y se arranca el servidor
const port = process.env.PORT || 3000;
const start = async () => {
    try {
        await mongoose.connect(process.env.MONGO_URI);
        app.listen(port, () => {
            console.log(`App esta funcionando en el puerto ${port}...`);
        });
    } catch (error) {
        console.log(error)
    }
};

start();
