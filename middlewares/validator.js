// Middleware para la validacion con Joi de la ruta '/'
const listValidator = (schema) => {
    return async (ctx, next) => {
        try {
            await next()
            schema.validateAsync({ page: ctx.request.header.page })
            console.log('pase el middleware');
        } catch (error) {
            console.log(error);
        }
    }
}
// Middleware para la validacion con Joi de la ruta '/update'
const updateValidator = (schema) => {
    return async (ctx, next) => {
        try {
            await next()
            schema.validateAsync({
                movie: ctx.request.body.movie,
                find: ctx.request.body.find,
                replace: ctx.request.body.replace
            })
            console.log('pase el middleware');
        } catch (error) {
            console.log(error);
        }
    }
}
// Middleware para la validacion con Joi de la ruta '/:title?'
const findValidator = (schema) => {
    return async (ctx, next) => {
        try {
            await next()
            schema.validateAsync({ title: ctx.request.params.title, y: ctx.request.header.y })
            console.log('pase el middleware');
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = {
    listValidator,
    updateValidator,
    findValidator
}