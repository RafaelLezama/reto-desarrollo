// Modelo de Mongoose para la base de datos
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const movieSchema = new mongoose.Schema({
    title: String,
    year: String,
    released: String,
    genre: [],
    director: String,
    actors: [],
    plot: String,
    ratings: []
});
movieSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Movie', movieSchema);