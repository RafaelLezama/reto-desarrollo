// se importa e inicializa el paquete koa-router
const Router = require('koa-router');
const router = new Router();
// Se importan los controladores
const {
    listController,
    findController,
    updateController
} = require('../controllers/movieController');
// Se importan los esquemas de validacion
const {
    findSchema,
    listSchema,
    updateSchema
} = require('../schemas/movieSchema');
// Se importa el middleware de validacion
const {
    listValidator,
    findValidator,
    updateValidator
} = require('../middlewares/validator')

// Rutas de la app
router.get('/', listValidator(listSchema), listController);
router.get('/:title?', findValidator(findSchema), findController);
router.post('/update', updateValidator(updateSchema), updateController);

module.exports = router;