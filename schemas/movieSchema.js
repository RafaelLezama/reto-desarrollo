const Joi = require('joi');
// Esquema de validacion para la ruta '/:title?'
const findSchema = Joi.object({
    title: Joi.string()
        .min(3)
        .max(30)
        .required(),

    y: Joi.number()
        .integer()
        .min(1800)
        .max(2022)
        .allow('')

});
// Esquema de validacion para la ruta '/'
const listSchema = Joi.object({
    page: Joi.string()
        .min(0)
        .max(50),

});
// Esquema de validacion para la ruta '/update'
const updateSchema = Joi.object({
    movie: Joi.string()
        .alphanum()
        .max(30)
        .required(),

    find: Joi.string()
        .alphanum()
        .max(30)
        .required(),

    replace: Joi.string()
        .alphanum()
        .max(30)
        .required()
});


module.exports = {
    findSchema,
    listSchema,
    updateSchema
};

